import axios from 'axios'
export const API_URL = process.env.REACT_APP_API_URL;
export const API_KEY = process.env.REACT_APP_API_KEY;

export const getWeatherForecast = (searchTerm, cntLimit) => {
    return axios.get(`${API_URL}/forecast?q=${searchTerm}&appid=${API_KEY}&units=metric&cnt=${cntLimit}`)
        .then(response => {
            return response.data
        })
};
