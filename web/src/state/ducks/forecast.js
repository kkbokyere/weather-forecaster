import {getWeatherForecast} from "@api/forecast";

export const GET_FORECAST_SUCCESS = "GET_FORECAST_SUCCESS";
export const GET_FORECAST_FAILURE = "GET_FORECAST_FAILURE";
export const GET_FORECAST_REQUEST = "GET_FORECAST_REQUEST";

export const initialState = {
    isLoading: false,
    error: null,
    data: []
};

function getForecastRequest(payload) {
    return {
        type: GET_FORECAST_REQUEST,
        payload
    }
}

function getForecastSuccess(payload) {
    return {
        type: GET_FORECAST_SUCCESS,
        payload
    }
}

function getForecastFailure(payload) {
    return {
        type: GET_FORECAST_FAILURE,
        payload
    }
}

export function fetchForecast(searchTerm, noOfDays = 3) {
    function calculateCntLimit(limit) {
        //24 hours in a day / 3 hour responses
        return (24/3 * limit)
    }

    return function (dispatch) {
        dispatch(getForecastRequest(searchTerm));
        return getWeatherForecast(searchTerm, calculateCntLimit(noOfDays))
            .then(response => {
                dispatch(getForecastSuccess(response))
            }).catch(error => {
                dispatch(getForecastFailure(error.response.data))
            })
    }
}

export default (state = initialState, action) => {
    let { payload } = action;
    switch (action.type) {
        case GET_FORECAST_SUCCESS:
            return {
                ...state,
                data: [...state.data, payload],
                isLoading: false
            };
        case GET_FORECAST_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: payload
            };
        case GET_FORECAST_REQUEST:
            return {
                ...state,
                error: null,
                isLoading: true
            };
        default:
            return state
    }
}
