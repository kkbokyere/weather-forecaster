import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import axiosMock from "axios";
import reducer, { GET_FORECAST_REQUEST, GET_FORECAST_SUCCESS, initialState, GET_FORECAST_FAILURE, fetchForecast } from './forecast'

import {mockResponse} from "../../views/tests/__mocks__/data";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('forecasts reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            data: [],
            isLoading: false,
            error: null
        })
    });

    it('should handle GET_FORECAST_REQUEST', () => {
        expect(
            reducer(initialState, {
                type: GET_FORECAST_REQUEST,
                payload: "london"
            })
        ).toEqual(
            {error: null, isLoading: true, data: []}
        )
    });

    it('should handle GET_FORECAST_SUCCESS', () => {
        expect(
            reducer(initialState, {
                type: GET_FORECAST_SUCCESS,
                payload: mockResponse
            })
        ).toEqual({error: null, isLoading: false, data: [mockResponse]})
    });

    it('should handle GET_FORECAST_FAILURE', () => {
        expect(
            reducer(initialState, {
                type: GET_FORECAST_FAILURE,
                payload: {
                    message: 'some error'
                }
            })
        ).toEqual(
            {error: {
                message: 'some error'
                }, isLoading: false, data: []}
        )
    });
});

describe('async actions', () => {
    it('creates GET_FORECAST_SUCCESS when fetching forecasts has been done', () => {
        axiosMock.get.mockResolvedValueOnce({ data: mockResponse });

        const expectedActions = [
            { type: GET_FORECAST_REQUEST, payload: 'london' },
            { type: GET_FORECAST_SUCCESS, payload: mockResponse }
        ];
        const store = mockStore(initialState);

        return store.dispatch(fetchForecast('london')).then(() => {
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions)
        })
    })
});
