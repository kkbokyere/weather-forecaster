import { createSelector } from 'reselect'
import moment from "moment";

const getForecasts = (state) => mapAllForecasts(state);

const mapAllForecasts = (state) => state.map(mapForecast);

const mapForecast = (payload) => ({
    city: {
        id: payload.city.id,
        name: payload.city.name,
        country: payload.city.country
    },
    temperatures: payload.list.map(({dt, dt_txt, main: { temp }}) => ({
        dt,
        dt_txt,
        main: {
            temp
        }
    }))
});

export const groupTemperaturesSelector = createSelector(
    [getForecasts],
    (forcasts) => forcasts.map(item => ({
        ...item,
        temperatures: item.temperatures.reduce((curr, item) => {
            const key = moment(item.dt_txt).format('dddd');
            if(!curr[key]) {
                curr[key] = []
            }
            curr[key].push({...item, dt_txt: moment(item.dt_txt).format('LT') });
            return curr
        }, {})
    })));

