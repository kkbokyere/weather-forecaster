import React from 'react';
import '@styles/App.scss';
import Layout from "@components/Layout";
import SearchInput from "@containers/SearchInput";
import ForecastList from "@containers/ForecastList";
import Errors from "@containers/Errors";
function App() {
  return (
    <div className="App">
        <Layout>
            <h1>Weather Forecaster</h1>
            <Errors/>
            <SearchInput/>
            <ForecastList/>
        </Layout>
    </div>
  );
}

export default App;
