import React from 'react';
import { render, fireEvent, cleanup, waitForElement } from './tests/helper'

import App from './App';
import {mockResponse} from "./tests/__mocks__/data";
import axiosMock from "axios";


describe('App Tests', () => {
  afterEach(cleanup);

  const setup = (props) => {
    return render(<App/>, props);
  };

  it('renders App title', () => {
    const { getByText } = setup();
    const linkElement = getByText("Weather Forecaster");
    expect(linkElement).toBeInTheDocument();
  });

  it('should render app', () => {
    const { asFragment } = setup();
    expect(asFragment()).toMatchSnapshot();
  });

  it('should render input elements', () => {
    const { getByTestId } = setup();
    expect(getByTestId('search-input')).toBeInTheDocument();
    expect(getByTestId('search-button')).toBeInTheDocument();
    expect(getByTestId("forecast-list").children.length).toBe(0);
  });

  it('should render forecastList when a valid searchs are made', async () => {
    axiosMock.get
        .mockResolvedValueOnce({ data: mockResponse })
        .mockResolvedValueOnce({ data: {...mockResponse, city: {
          id: 1234,
              name: 'Tottenham'
            }} });

    const { getByTestId, getByText } = setup({
      initialState: {
        forecast: {
          isLoading: false,
          error: null,
          data: []
        }
      }
    });
    const inputEl = getByTestId('search-input');
    const searchBtn = getByTestId('search-button');

    expect(getByTestId("forecast-list").children.length).toBe(0);

    //first search
    fireEvent.change(inputEl, { target: { value: "london"}});
    fireEvent.click(searchBtn);

    const resolvedList = await waitForElement(() => getByTestId("forecast-list"));
    expect(axiosMock.get).toHaveBeenCalledTimes(1);
    expect(getByText('London')).toBeInTheDocument();
    expect(getByText('12:00 AM : 15℃')).toBeInTheDocument();
    expect(resolvedList.children.length).toBe(1);
    expect(resolvedList).toBeInTheDocument();

    // second search
    fireEvent.change(inputEl, { target: { value: "tottenham"}});
    fireEvent.click(searchBtn);

    const updatedResolvedList = await waitForElement(() => getByTestId("forecast-list"));
    expect(getByText('Tottenham')).toBeInTheDocument();
    expect(updatedResolvedList.children.length).toBe(2);
  });
});
