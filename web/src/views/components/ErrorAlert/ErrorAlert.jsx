import React from 'react';
import MuiAlert from '@material-ui/lab/Alert';

import PropTypes from 'prop-types'

const ErrorAlert = ({ error }) => {
    if(!error) {
        return null
    }
    return (
        <MuiAlert data-testid='error-alert' severity="error" variant="filled" children={error.message}/>
    );
};

ErrorAlert.propTypes = {
    error: PropTypes.any,
};

export default ErrorAlert;
