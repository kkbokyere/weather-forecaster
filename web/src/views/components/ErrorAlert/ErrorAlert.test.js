import React from 'react';
import { render } from '@tests/helper'

import ErrorAlert from '@components/ErrorAlert';
describe('ErrorAlert Tests', () => {

  const setup = (props) => {
    return render(<ErrorAlert {...props}/>);
  };
  it('should render ErrorAlert', async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

  it('should render ErrorAlert with no message', async () => {
    const { container } = setup();

    expect(container).toBeEmpty();
  });

  it('should render ErrorAlert with message', async () => {
    const { getByText } = setup({ error: {
      message: 'some error'
      }});
    expect(getByText('some error')).toBeInTheDocument();
  });

});
