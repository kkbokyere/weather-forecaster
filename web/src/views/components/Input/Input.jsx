import React, {useState} from 'react';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

import styles from './Input.module.scss'

import PropTypes from 'prop-types';

const Input = ({ handleOnSubmit }) => {
    const [searchTerm, setSearchTerm] = useState('');
    const onSubmitForm = (e) => {
        handleOnSubmit(searchTerm);
        e.preventDefault();
    };
    const handleOnChange = (e) => {
        setSearchTerm(e.currentTarget.value)
    };
    return (
        <Paper data-testid="search-form" component="form" className={styles.inputContainer} onSubmit={onSubmitForm}>
            <input
                data-testid="search-input"
                onChange={handleOnChange}
                className={styles.inputBase}
                placeholder="Enter city or location"
            />
            <IconButton data-testid="search-button" type="submit" className={styles.iconButton} aria-label="search">
                <SearchIcon />
            </IconButton>
        </Paper>
    );
};

Input.propTypes = {
    handleOnSubmit: PropTypes.func
};

export default Input;
