import React from 'react';
import { render, fireEvent } from '@tests/helper'

import Input from '@components/Input';
describe('Input Tests', () => {

  const setup = (props) => {
    return render(<Input {...props}/>);
  };
  it('should render Input', async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

  it('should call handleOnClick function when submitting the form', async () => {
    const handleOnSubmit = jest.fn();
    const { getByTestId } = setup({
      handleOnSubmit
    });

    fireEvent.submit(getByTestId('search-form'));
    expect(handleOnSubmit).toHaveBeenCalled();
  });

  it('should call handleOnClick function when clicking the search button', async () => {
    const handleOnSubmit = jest.fn();
    const { getByTestId } = setup({
      handleOnSubmit
    });

    fireEvent.click(getByTestId('search-button'));
    expect(handleOnSubmit).toHaveBeenCalled();
  });


  it('should call handleOnClick function with search when clicking the search button', async () => {
    const handleOnSubmit = jest.fn();
    const { getByTestId } = setup({
      handleOnSubmit
    });
    const inputEl = getByTestId('search-input');
    fireEvent.change(inputEl, { target: { value: "london"}});
    fireEvent.click(getByTestId('search-button'));
    expect(handleOnSubmit).toHaveBeenCalledWith("london");
  });

});
