import React from 'react';
import PropTypes from 'prop-types'

import styles from './List.module.scss'
import ListItem from "@components/ListItem";
import {CircularProgress} from "@material-ui/core";

const List = ({ forecast = [], isLoading }) => {
    if(isLoading) {
        return <span data-testid="loading-spinner"><CircularProgress/></span>
    }
    return (
        <div className={styles.listContainer} data-testid="forecast-list">
            {forecast.map(item => <ListItem key={item.city.id} data={item}/>)}
        </div>
    );
};

List.propTypes = {
    forecast: PropTypes.array,
};

export default List;
