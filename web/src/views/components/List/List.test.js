import React from 'react';
import { render } from '@tests/helper'

import List from '@components/List';
describe('List Tests', () => {

  const mockData = [{
    city: {
      name:'London',
      id: 1
    },
    temperatures: {
      Friday: [{
        dt: 1598583600,
        dt_txt: "4:00 AM",
        main: {temp: 13.95}
      }]
    }
  },{
    city: {
      name:'Scotland',
      id: 2
    },
    temperatures: {
      Friday: [{
        dt: 1598583600,
        dt_txt: "3:00 AM",
        main: {temp: 13.95}
      }]
    }
  }];

  const setup = (props) => {
    return render(<List {...props}/>);
  };
  it('should render List', async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

  it('should render List with 1 items', async () => {
    const { asFragment, getByText, getByTestId } = setup({
      forecast: [mockData[0]]
    });
    expect(getByTestId('forecast-list').children.length).toBe(1);
    expect(getByText('London')).toBeInTheDocument();
    expect(getByText('4:00 AM : 14℃')).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
  });

  it('should render List with multiple items', async () => {
    const { asFragment, getByText, getByTestId } = setup({
      forecast: mockData
    });
    expect(getByTestId('forecast-list').children.length).toBe(2);
    expect(getByText('London')).toBeInTheDocument();
    expect(getByText('Scotland')).toBeInTheDocument();
    expect(getByText('3:00 AM : 14℃')).toBeInTheDocument();
    expect(getByText('4:00 AM : 14℃')).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
  });
});
