import React, {useState} from 'react';
import { v4 as uuidv4 } from 'uuid';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Card from '@material-ui/core/Card';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import {Collapse} from "@material-ui/core";

import styles from './ListItem.module.scss'
import TemperatureCard from "@components/TemperatureCard";

const ListItem = ({ data }) => {
    const { city, temperatures } = data;
    const [expanded, setExpanded] = useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    return (
        <Card className={styles.listItem}>
            <div className={styles.listItemHeader}>
                <img
                    className={styles.listItemCountryImg}
                    alt={`${city.name}`}
                    src={`http://catamphetamine.gitlab.io/country-flag-icons/3x2/${city.country}.svg`}/>
            <h3 className={styles.listItemTitle}>{city.name}</h3>
            <CardActions>
                <IconButton
                    className={cx(styles.expand, {
                        [styles.expandIsOpen]: expanded,
                    })}
                    onClick={handleExpandClick}>
                    <ExpandMoreIcon/>
                </IconButton>
            </CardActions>
            </div>
            <Collapse in={expanded} timeout="auto">
                <div className={styles.listItemCollapse}>
                    {Object.keys(temperatures).map((day) => {
                        const dayTempList = temperatures[day];
                        return (
                            <div key={uuidv4()}>
                                <h3>{day}</h3>
                                {dayTempList && dayTempList.map((item) => <TemperatureCard key={uuidv4()} date={item.dt_txt} temperature={item.main.temp}/>)}
                            </div>)
                    })}
                </div>
            </Collapse>
        </Card>
    );
};

ListItem.propTypes = {
    data: PropTypes.shape({
        city: PropTypes.object,
        temperatures: PropTypes.object,
    })
};

export default ListItem;
