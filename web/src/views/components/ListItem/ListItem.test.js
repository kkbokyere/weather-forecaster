import React from 'react';
import { render } from '@tests/helper'

import ListItem from '@components/ListItem';
describe('ListItem Tests', () => {


  const setup = (props) => {
    return render(<ListItem {...props}/>);
  };
  it('should render ListItem', async () => {
    const { asFragment } = setup({
      data: {
        city:{},
        temperatures: {}
      }
    });

    expect(asFragment()).toMatchSnapshot();
  });

  it('should render ListItem with data', async () => {
    const { asFragment, getByText } = setup({
      data: {
        city:{
          name: 'London'
        },
        temperatures: {
          Friday: [{
            dt: 1598583600,
            dt_txt: "3:00 AM",
            main: {temp: 13.95}
          }]
        }
      }
    });
    expect(getByText('London')).toBeInTheDocument();
    expect(getByText('Friday')).toBeInTheDocument();
    expect(getByText('3:00 AM : 14℃')).toBeInTheDocument();
    expect(asFragment()).toMatchSnapshot();
  });
});
