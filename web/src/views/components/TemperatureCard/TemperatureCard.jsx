import React from 'react';
import PropTypes from 'prop-types'

import styles from './TemperatureCard.module.scss'

const TemperatureCard = ({ date, temperature }) => {
    return (
        <div className={styles.listContainer}>
            {date} : {Math.round(temperature)}&#8451;
        </div>
    );
};

TemperatureCard.propTypes = {
    temperature: PropTypes.number,
    date: PropTypes.any,
};

export default TemperatureCard;
