import { connect } from 'react-redux'
import ErrorAlert from "@components/ErrorAlert";
const mapStateToProps = ({ forecast }) => ({
    error: forecast.error
});

export default connect(
    mapStateToProps,
    null
)(ErrorAlert)
