import React from 'react';
import { render } from '@tests/helper'

import Errors from '@containers/Errors';
describe('Errors Tests', () => {

  const setup = (props) => {
    return render(<Errors />, props);
  };
  it('should render Errors', async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

  it('should render Errors with message', async () => {
    const { getByText } = setup({
      initialState: {
        forecast: {
          error: {
            message: 'city not found'
          }
        }
      }
    });
    expect(getByText('city not found')).toBeInTheDocument();
  });

});
