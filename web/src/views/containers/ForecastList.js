import { connect } from 'react-redux'
import List from "@components/List";
import {groupTemperaturesSelector} from "@state/ducks/selectors";
const mapStateToProps = ({ forecast }) => {
    return {
        forecast: groupTemperaturesSelector(forecast.data)
    }
};

export default connect(
    mapStateToProps,
    null
)(List)
