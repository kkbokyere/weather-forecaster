import React from 'react';
import { render } from '@tests/helper'

import ForecastList from '@containers/ForecastList';
import {mockResponse} from "../tests/__mocks__/data";
describe('ForecastList Tests', () => {

  const setup = (props) => {
    return render(<ForecastList />, props);
  };
  it('should render ForecastList', async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

  it('should render ForecastList', async () => {
    const { getByText, getByTestId } = setup({
      initialState: {
        forecast: {
          data: [mockResponse]
        }
      }
    });
    expect(getByTestId('forecast-list').children.length).toBe(1);
    expect(getByText('London')).toBeInTheDocument();
    expect(getByText('Friday')).toBeInTheDocument();
    expect(getByText('12:00 AM : 15℃')).toBeInTheDocument();
  });

});
