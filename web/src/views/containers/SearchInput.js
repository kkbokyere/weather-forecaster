import { connect } from 'react-redux'
import Input from "@components/Input";
import {fetchForecast} from "@state/ducks/forecast";

const mapDispatchToProps = dispatch => ({
    handleOnSubmit: data => {
        dispatch(fetchForecast(data))
    }
});

export default connect(
    null,
    mapDispatchToProps
)(Input)
