import React from 'react';
import { render } from '@tests/helper'

import SearchInput from '@containers/SearchInput';
describe('SearchInput Tests', () => {

  const setup = (props) => {
    return render(<SearchInput />, props);
  };
  it('should render SearchInput', async () => {
    const { asFragment } = setup();

    expect(asFragment()).toMatchSnapshot();
  });

});
