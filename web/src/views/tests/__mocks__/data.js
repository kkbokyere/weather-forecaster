export const mockResponse = {
    "cod": "200",
    "message": 0,
    "cnt": 24,
    "city": {
        "id": 2643743,
        "name": "London",
        "coord": {
            "lat": 51.5085,
            "lon": -0.1257
        },
        "country": "GB",
        "population": 1000000,
        "timezone": 3600,
        "sunrise": 1598504752,
        "sunset": 1598554707
    },
    list: [{
        "dt": 1598572800,
        "main": {
            "temp": 14.74,
            "feels_like": 14.06,
            "temp_min": 14.56,
            "temp_max": 14.74,
            "pressure": 1003,
            "sea_level": 1003,
            "grnd_level": 1000,
            "humidity": 84,
            "temp_kf": 0.18
        },
        "weather": [{
            "id": 803,
            "main": "Clouds",
            "description": "broken clouds",
            "icon": "04n"
        }],
        "clouds": {
            "all": 76
        },
        "wind": {
            "speed": 1.89,
            "deg": 259
        },
        "visibility": 10000,
        "pop": 0.24,
        "sys": {
            "pod": "n"
        },
        "dt_txt": "2020-08-28 00:00:00"
    }]
};
